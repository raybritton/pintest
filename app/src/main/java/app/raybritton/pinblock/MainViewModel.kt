package app.raybritton.pinblock

import android.os.Bundle
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel(), MainMvvm.ViewModel {
    private var view: MainMvvm.View? = null
    private var pin: String? = null
    private var encoded: String? = null

    override fun setup(view: MainMvvm.View, savedState: Bundle?) {
        this.view = view
        pin = savedState?.getString(STATE_PIN)
        encoded = savedState?.getString(STATE_ENCODED)
        if (pin != null) {
            view.showPin(pin!!)
        }
        if (encoded != null) {
            view.showResult(encoded!!)
        }
    }

    override fun encode(pin: String) {
        if (pin.length < BuildConfig.PIN_LENGTH_MIN) {
            view?.showError(MainMvvm.Error.TooShort)
            return
        }
        if (pin.length > BuildConfig.PIN_LENGTH_MAX) {
            view?.showError(MainMvvm.Error.TooLong)
            return
        }
        if (pin.any { !it.isDigit() }) {
            view?.showError(MainMvvm.Error.NotAllDigits)
            return
        }

        encoded = generateFormat3PinBlock(pin, BuildConfig.PAN)
        view?.showResult(encoded!!)
    }

    override fun saveState(outState: Bundle) {
        outState.putString(STATE_PIN, pin)
        outState.putString(STATE_ENCODED, encoded)
    }

    override fun clearUp() {
        view = null
    }

    companion object {
        private const val STATE_PIN = "pin.string?"
        private const val STATE_ENCODED = "encoded.string?"
    }
}