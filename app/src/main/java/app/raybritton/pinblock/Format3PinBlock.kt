package app.raybritton.pinblock

import java.util.*
import kotlin.experimental.xor
import kotlin.random.Random

private fun makeByte(hiNibble: Byte, lowNibble: Byte) = lowNibble.withHiNibble(hiNibble)

private fun Byte.withHiNibble(value: Byte): Byte =
    ((0xF0 and (value.toInt() shl 4)) or this.toInt()).toByte()

private fun Byte.withLowNibble(value: Byte): Byte =
    ((0x0F and value.toInt()) or this.toInt()).toByte()

private fun String.toNibbles() = this
    .map { Character.getNumericValue(it).toByte() }
    .chunked(2)
    .map { makeByte(it[0], it[1]) }

/**
 * Convert string PIN and PAN to encoded ISO 9564 Format 3 PIN Block
 *
 * @see <a href="https://en.wikipedia.org/wiki/ISO_9564">Wikipedia</a>
 */
fun generateFormat3PinBlock(pin: String, pan: String): String {
    var epin = ("3" + pin.length + pin)
    while (epin.length < 16) {
        epin += "%x".format(Random.nextInt(10,15))
    }
    val epan = "0000" + pan.drop(3).dropLast(1)

    val pinNibs = epin.toNibbles()
    val panNibs = epan.toNibbles()

    return pinNibs
        .mapIndexed { idx, nibble -> nibble xor panNibs[idx] }
        .map { "%x".format(it) }
        .joinToString("")
        .padStart(16, '0')
        .toUpperCase(Locale.US)
}