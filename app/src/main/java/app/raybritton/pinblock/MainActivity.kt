package app.raybritton.pinblock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainMvvm.View {

    private val viewModel: MainMvvm.ViewModel by lazy { ViewModelProviders.of(this)[MainViewModel::class.java] }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.setup(this, savedInstanceState)

        main_input.hint = getString(
            R.string.main_input_hint,
            BuildConfig.PIN_LENGTH_MIN,
            BuildConfig.PIN_LENGTH_MAX
        )

        main_input.addTextChangedListener {
            main_input.error = null
        }

        main_input.setOnEditorActionListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == EditorInfo.IME_ACTION_DONE) {
                viewModel.encode(main_input.text.toString())
                true
            } else {
                false
            }
        }

        main_execute.setOnClickListener {
            viewModel.encode(main_input.text.toString())
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        viewModel.saveState(outState)
        super.onSaveInstanceState(outState)
    }

    override fun showResult(encoded: String) {
        main_output.text = getString(R.string.main_output, encoded)
    }

    override fun showPin(pin: String) {
        main_input.setText(pin)
    }

    override fun showError(type: MainMvvm.Error) {
        main_input.error = when (type) {
            MainMvvm.Error.NotAllDigits -> getString(R.string.main_pin_error_not_all_digits)
            MainMvvm.Error.TooShort -> getString(R.string.main_pin_error_too_short, BuildConfig.PIN_LENGTH_MIN)
            MainMvvm.Error.TooLong -> getString(R.string.main_pin_error_too_long, BuildConfig.PIN_LENGTH_MAX)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.clearUp()
    }
}
