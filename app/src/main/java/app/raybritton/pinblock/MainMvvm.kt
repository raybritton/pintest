package app.raybritton.pinblock

import android.os.Bundle

interface MainMvvm {
    enum class Error {
        NotAllDigits,
        TooShort,
        TooLong
    }

    interface View {
        fun showResult(encoded: String)
        fun showPin(pin: String)
        fun showError(type: Error)
    }

    interface ViewModel {
        fun setup(view: View, savedState: Bundle?)
        fun encode(pin: String)
        fun clearUp()
        fun saveState(outState: Bundle)
    }
}